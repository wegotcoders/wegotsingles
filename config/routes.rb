Rails.application.routes.draw do
  devise_for :customers, controllers: { registrations: "registrations" }
  root 'pages#home'

  resources :subscribers

  resources :profiles do
    collection do
      get :search, :height
    end

    member do
      get :add_language
    end
  end

  resources :messages, only: [ :show, :create, :index] do
    member do
      get :new_reply
    end
  end
end
