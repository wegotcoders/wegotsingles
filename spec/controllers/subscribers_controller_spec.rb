require 'rails_helper'
include Devise::TestHelpers

RSpec.describe SubscribersController, type: :controller do

  describe "POST to create" do
    before do


      @customer = Customer.create!(:username => "SimonKnight", :email => "simon@knight.com", :first_name => "Simon", :last_name => "Knight", :password => "password", :password_confirmation => "password", date_of_birth: Date.parse("Sat, 03 Dec 1926"))
      sign_in @customer

      allow(Stripe::Customer).to receive(:create).with({
        email: 'dsfdgf@asdf.com',
        source: 'tok_17EMPPFAxDhFQV3qsjD8EQ92',
        plan: 1
      })
      # binding.pry
      params = {
                :authenticity_token => "abcd",
                :stripeToken        => "tok_17EMPPFAxDhFQV3qsjD8EQ92",
                :stripeTokenType    => "card",
                :stripeEmail        => "dsfdgf@asdf.com",
                :controller         => "subscribers",
                :action             => "create"
                }

      post :create, params

    end

    it "sets the account to premium" do
      expect(@customer.reload.subscriber).to be true
    end
  end

end
