require "rails_helper"

RSpec.describe Notifier, type: :mailer do
  before do
    @sender = Customer.create!(first_name: "Bob", last_name: "Smith", username: "sexybob", email: "test1@test.com", password: "password1", password_confirmation: "password1", date_of_birth: Date.parse("Sat, 03 Dec 1972"))
    @recipient = Customer.create!(first_name: "Sarah", last_name: "Jane", username: "sexyblah", email: "test2@test.com", password: "password2", password_confirmation: "password2", date_of_birth: Date.parse("Sat, 03 Dec 1972"))
    @message = Message.create!(subject: "Hello", body: "blah blah blah", sender: @sender, recipient: @recipient)
  end

  it "alerts the recipient by email when a new message is received" do
    expect(ActionMailer::Base.deliveries.length).to eq 1
    expect(ActionMailer::Base.deliveries.last.subject).to eq "You've received a new message on We Got Singles!"
    expect(ActionMailer::Base.deliveries.last.body.encoded).to have_content message_path(@message)
  end
end
