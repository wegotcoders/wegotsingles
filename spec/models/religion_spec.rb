require 'rails_helper'

RSpec.describe Religion, type: :model do
  before do

    @profile_1 = Profile.new(age: 99)
    @profile_2 = Profile.new(age: 53)
    @religion = Religion.create!(name: "Christianity")
    @profile_1.religion = @religion
    @profile_1.save!
    @profile_2.religion = @religion
    @profile_2.save!
  end

  it "is connected to profiles" do
    expect(@profile_1.religion).to eq @religion
    expect(@religion.profiles.length).to eq 2
  end
end
