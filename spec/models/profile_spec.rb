require 'rails_helper'

RSpec.describe Profile, :type => :model do

  before do
    @customer = Profile.create!(:biography => "oudhiuds")
    @customer1 = Profile.create!(:age => 30, :biography => "oudhiuds")
    @attributes = @customer.attributes
  end

  describe 'Gelocation' do

    before do

      Geocoder::Lookup::Test.add_stub(
        "London, UK", [
          {
            'latitude'     => 51.5073509,
            'longitude'    => -0.1277583,
            'address'      => 'London, UK',
            'state'        => 'London',
            'state_code'   => '',
            'country'      => 'UK',
            'country_code' => 'UK'
          }
        ]
      )

      # @profile = Profile.create!(city: "London", country: "UK")
      @customer = Customer.create!(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, type: "Female", email: "test1@test.com", password: "password1", password_confirmation: "password1", date_of_birth: Date.parse("Sat, 03 Dec 1972"), :username => "username2")

      @customer.profile.update!(age: 43, industry: "Computing", city: "London", country: "UK")
      @profile = @customer.profile
    end

    it "builds a string suitable for geocoding" do
      expect(@profile.geocode_string).to eq('London, UK')
    end

    it "geocodes the co-ordinates" do
      @profile.geocode

      expect(@profile.latitude).to eq(51.5073509)
      expect(@profile.longitude).to eq(-0.1277583)
    end

    it "geocodes on save" do
      @profile.save!
      expect(@profile.latitude).to eq(51.5073509)
      expect(@profile.longitude).to eq(-0.1277583)
    end
  end

  describe 'Percentage Profile Completed' do

    before do

        @customer = Customer.create!(first_name: "Simon", last_name: "Knight", type: "Female", email: "test1@test.com", password: "password1", password_confirmation: "password1", date_of_birth: Date.parse("Sat, 03 Dec 1986"), :username => "username2")
        @customer.profile.update!(:biography => "oudhiuds", :age => 29)
        @profile = @customer.profile

        @customer1 = Customer.create!(first_name: "Simon", last_name: "Knight", type: "Female", email: "test2@test.com", password: "password1", password_confirmation: "password1", date_of_birth: Date.parse("Sat, 03 Dec 1986"), :username => "username3")

        @customer1.profile.update!(:biography => "oudhiuds")
        @profile1 = @customer1.profile

        @attributes = @profile.attributes

    end

    it "should count the number of attributes in the profile model" do
      expect(@attributes.length).to eq(Profile.column_names.length)
    end

    it "should return the percentage profile completed" do
      expect(@customer.profile.profile_completed).to eq(18)
      expect(@customer1.profile.profile_completed).to eq(9)
      expect(@customer1.profile.profile_completed).not_to eq(0)
    end
  end

  describe 'Languages in the profile' do
    describe "Adding valid languages" do
      before do
        @language = Language.new(:name => "English")
        @customer.languages << @language
        @customer.save!
      end

      it "adds one language to the profile" do
        expect(@customer.languages.length).to eq(1)
      end
    end

    describe "Remove blank languages" do
      before do
        # @language = Language.new(:name => "")
        Profile.create!(:profile_languages_attributes => [:language_attributes => {:name => ""}])
        # @customer.languages << @language
        # @customer.save!
      end

      it "removes any blank language objects before they are saved to the database" do
        expect(@customer.languages.length).to eq(0)
      end
    end
  end

  describe "profile image" do
    before do
      WebMock.enable!
      stub_request(:get, "http://api.randomuser.me/?gender=female").
        to_return(:status => 200, :body => File.read('spec/fixtures/female.json'), :headers => {})

      @profile = Female.create!(:username => "sassy123",
        :email => Faker::Internet.email, :password => "password",
        :password_confirmation => "password", date_of_birth: Date.parse("Sat, 03 Dec 1972")).profile
    end

    it "retreives a profile picture from the web on create" do
      expect(@profile.img_url).to eq("https://randomuser.me/api/portraits/med/women/69.jpg")
      expect(@profile.thumb_url).to eq("https://randomuser.me/api/portraits/thumb/women/69.jpg")
    end

    after do
      WebMock.disable!
    end
  end
end
