require 'rails_helper'

RSpec.describe Message, type: :model do
  before do

    allow_any_instance_of(Profile).to receive(:set_profile_image)

    @sender = Customer.create!(first_name: "Bob", last_name: "Smith", username: "sexybob", email: "test1@test.com", password: "password1", password_confirmation: "password1", date_of_birth: Date.parse("Sat, 03 Dec 1980"))
    @recipient = Customer.create!(first_name: "Sarah", last_name: "Jane", username: "sexyblah", email: "test2@test.com", password: "password2", password_confirmation: "password2", date_of_birth: Date.parse("Sat, 03 Dec 1980"))

    @message = Message.create!(subject: "Hello", body: "blah blah blah", sender: @sender, recipient: @recipient)
  end

  it "is sent by a customer and received by a customer" do
    expect(@message.sender).to eq @sender
    expect(@message.recipient).to eq @recipient
  end
end
