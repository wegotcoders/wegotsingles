# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Religion.create!(name: "Buddhism")
Religion.create!(name: "Christianity")
Religion.create!(name: "Judaism")
Religion.create!(name: "Scientology")
Religion.create!(name: "Rastafarianism")
Religion.create!(name: "Atheism")
Religion.create!(name: "Agnosticism")
Religion.create!(name: "Hinduism")
Religion.create!(name: "Islam")
Religion.create!(name: "Confucianism")
Religion.create!(name: "Taoism")
Religion.create!(name: "Paganism")
Religion.create!(name: "Wicca")
Religion.create!(name: "Sikhism")
Religion.create!(name: "Mormonism")
