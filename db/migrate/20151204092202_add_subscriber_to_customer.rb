class AddSubscriberToCustomer < ActiveRecord::Migration
  def change
    add_column :customers, :subscriber, :boolean
  end
end
