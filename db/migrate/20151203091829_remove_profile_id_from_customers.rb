class RemoveProfileIdFromCustomers < ActiveRecord::Migration
  def change
    remove_column :customers, :profile_id
  end
end
