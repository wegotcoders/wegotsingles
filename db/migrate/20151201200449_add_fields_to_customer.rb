class AddFieldsToCustomer < ActiveRecord::Migration
  def change
    add_column :customers, :first_name, :string
    add_column :customers, :last_name, :string
    add_column :customers, :type, :string
    add_column :customers, :profile_id, :integer
  end
end
