class AddReligionIdToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :religion_id, :integer
  end
end
