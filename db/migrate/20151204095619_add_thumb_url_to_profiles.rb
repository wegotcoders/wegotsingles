class AddThumbUrlToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :thumb_url, :string
  end
end
