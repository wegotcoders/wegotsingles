class AddRespondsToIdToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :responds_to_id, :integer
  end
end
