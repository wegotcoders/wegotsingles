class RemoveReligionFromProfile < ActiveRecord::Migration
  def change
    remove_column :profiles, :religion
  end
end
