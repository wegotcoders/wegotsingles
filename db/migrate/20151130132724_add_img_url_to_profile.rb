class AddImgUrlToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :img_url, :string
  end
end
