class ChangeLatitutdeAndLongitudeToDecimal < ActiveRecord::Migration
  def change
    change_column :profiles, :latitude, :decimal
    change_column :profiles, :longitude, :decimal
  end
end
