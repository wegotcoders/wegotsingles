class AddNameAndAgeToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :name, :string
    add_column :profiles, :age, :integer
  end
end
