class AddStarSignToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :star_sign, :integer
  end
end
