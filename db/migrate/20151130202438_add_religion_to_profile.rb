class AddReligionToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :religion, :integer
  end
end
