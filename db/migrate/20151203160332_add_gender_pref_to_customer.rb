class AddGenderPrefToCustomer < ActiveRecord::Migration
  def change
    add_column :customers, :gender_pref, :string
  end
end
