class ChangeEthnicityToIntegerInProfile < ActiveRecord::Migration
  def change
    change_column :profiles, :ethnicity, "integer USING ethnicity::integer"
  end
end
