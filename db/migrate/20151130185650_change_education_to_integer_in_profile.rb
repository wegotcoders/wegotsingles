class ChangeEducationToIntegerInProfile < ActiveRecord::Migration
  def change
    change_column :profiles, :education, "integer USING education::integer"
  end
end
