module Conversions
  module HeightConversions
    CMS_IN_FEET = 0.0328084
    INCHES_IN_CM = 2.54
    INCHES_IN_A_FOOT = 12

    def self.included(klass)
      klass.class_eval do
        before_validation :convert_height

        def convert_height
          if @height_feet && @height_inches
            self.height = Conversions::HeightConversions.convert_to_cm(@height_feet, @height_inches)
          end
        end
      end
    end

    def self.convert_to_cm(feet, inches)
      inches = ( feet.to_i * INCHES_IN_A_FOOT ) + inches.to_i
      (inches * INCHES_IN_CM).to_i
    end

    def self.convert_to_feet(height)
      height * CMS_IN_FEET
    end

    def feet_and_inches(height)
      feet = Conversions::HeightConversions.convert_to_feet(height)
      return feet.to_i, feet.remainder(feet) * 12
    end

  end
end