module ProfileCompletedness
  def self.fields_used_in_profile_completed
    # Add profile fields you want white listed to the white_listed array
    %w(age industry biography education smoker drinks religion_id height city country star_sign)
  end

  def white_listed_attributes
    attributes.select do |k,v|
      ProfileCompletedness.fields_used_in_profile_completed.include? k
    end
  end

  def completed_fields
    white_listed_attributes.inject(0) do |sum, pair|
      sum += 1 unless pair.last.blank?
      sum
    end
  end

  def profile_completed
    (( completed_fields / white_listed_attributes.length.to_f ) * 100 ).round
  end
end
