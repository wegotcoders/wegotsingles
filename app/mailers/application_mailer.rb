class ApplicationMailer < ActionMailer::Base
  default from: "admin@wegotsingles.com"
  layout 'mailer'
end
