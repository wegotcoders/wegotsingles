class Notifier < ApplicationMailer

  def new_message(sender, recipient, message)
    @sender = sender
    @recipient = recipient
    @message = message
    mail(to: recipient.email, subject: "You've received a new message on We Got Singles!")
  end

end
