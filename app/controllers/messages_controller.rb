class MessagesController < ApplicationController

  before_filter :find_message, only: [ :show, :new_reply]

  def new_reply
    @message_reply = Message.new
  end

  def create
    id = params[:message][:responds_to_id]
    @message = Message.find(id)

    @message_reply = Message.new(message_params)
    @message_reply.assign_attributes(sender: @message.recipient, recipient: @message.sender, responds_to: @message)
    @message_reply.save!

    redirect_to messages_path
  end

  def index
    @messages = Message.where(:recipient_id => current_customer.id).order('created_at DESC')
  end

  def show
    @message.update!(read: true)
  end

  private
  def find_message
    @message = Message.find(params[:id])
  end

  def message_params
    params.require(:message).permit(:subject, :body)
  end
end
