class ProfilesController < ApplicationController

  before_filter :find_profile, :only => [:show, :edit, :update]

  def index
    @profiles = Profile.all
  end

  def edit
    @religion_options = Religion.pluck(:name, :id).prepend(["", 0])
  end

  def add_language
    @language = Language.new
  end

  def search
    @customers = Customer.where(type: current_customer.gender_pref)
    @profiles = Profile.joins(:customer).where("customers.type = ?", current_customer.gender_pref)


    unless params[:industry].empty?
      @profiles = @profiles.where(industry: params[:industry])
    end

    unless params[:ethnicity].empty?
      @profiles = @profiles.where(ethnicity: params[:ethnicity])
    end

    unless params[:distance].empty?
      @profiles = @profiles.near([current_customer.profile.longitude, current_customer.profile.latitude], params[:distance])
    end

    @profiles = @profiles.where("age >= ?", params[:min_age]) unless params[:min_age].empty?
    @profiles = @profiles.where("age <= ?", params[:max_age]) unless params[:max_age].empty?

  end

  def update
    @profile.update!(profile_params)
    redirect_to root_path
  end

  def height
    @form = params[:form]
    @measurement = params[:measurement]
  end

  private
  def profile_params
    params.require(:profile).permit(:biography, :education, :smoker, :religion_id,
                                    :drinks, :height, :height_feet, :height_inches, :star_sign,
                                    :profile_languages_attributes => [:id, :language_attributes =>[:id, :name]])
  end

  def find_profile
    @profile = Profile.find(params[:id])
  end
end
