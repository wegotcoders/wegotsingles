class SubscribersController < ApplicationController
  before_filter :authenticate_customer!
  def new
  end

  def create

    begin

    customer = Stripe::Customer.create(
      :email   => params[:stripeEmail],
      :source  => params[:stripeToken],
      :plan    => 1
    )

    rescue Stripe::CardError => e
      flash[:error] = e.message
      redirect_to new_charge_path
    end
    current_customer.update!(:subscriber => true)
    redirect_to profile_path(current_customer.profile)
      flash[:notice] = "Thanks for joining WeGotSingles as a Gold Member"
  end
end
