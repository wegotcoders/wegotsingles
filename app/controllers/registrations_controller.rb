class RegistrationsController < Devise::RegistrationsController
  
  protected
  def after_sign_up_path_for(resource)
    
    if resource.type == "Female"
      profile_path(resource.profile)
    else
      new_subscriber_path
    end
  end
end