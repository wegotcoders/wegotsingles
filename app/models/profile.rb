class Profile < ActiveRecord::Base

  include ProfileCompletedness
  include Conversions::HeightConversions

  geocoded_by :geocode_string
  before_save :geocode, :if => Proc.new { |profile| profile.city_changed? || profile.country_changed? }
  before_save :set_profile_image, :unless => lambda { |o|
    # Only set the image if we have a customer and haven't already got an image
    customer.nil? || o.img_url || o.thumb_url
  }

  has_many :profile_languages
  has_many :languages, :through => :profile_languages
  accepts_nested_attributes_for :profile_languages, :reject_if => proc{ |attrs| attrs[:language_attributes][:name].blank? }

  belongs_to :religion
  belongs_to :customer
  attr_writer :height_feet, :height_inches

  enum education: [ :secondary, :college, :university ]
  enum drinks: [ :Never, :Socially, :Often, :Like_a_fiend]
  enum star_sign: [ :aries, :taurus, :gemini, :cancer, :leo, :virgo, :libra,
                    :scorpio, :sagittarius, :capricorn, :aquarius, :pisces ]
  enum ethnicity: [ :Asian, :Black, :Caucasian, :Hispanic ]

  def geocode_string
    "#{city}, #{country}"
  end

  def height_feet
    height ? feet_and_inches(height)[0] : ""
  end

  def height_inches
    height ? feet_and_inches(height)[1] : ""
  end

  private
  def set_profile_image
    response = HTTParty.get("http://api.randomuser.me/?gender=#{customer.gender}")
    punter_info = JSON(response.body)
    images = punter_info["results"][0]["user"]["picture"]
    self.img_url = images["medium"]
    self.thumb_url = images["thumbnail"]
  end
end
