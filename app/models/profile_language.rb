class ProfileLanguage < ActiveRecord::Base
  belongs_to :language
  belongs_to :profile

  accepts_nested_attributes_for :language
end
