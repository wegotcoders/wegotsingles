class Message < ActiveRecord::Base
  belongs_to :sender, class_name: "Customer", foreign_key: :sender_id
  belongs_to :recipient, class_name: "Customer", foreign_key: :recipient_id

  belongs_to :responds_to, class_name: "Message", foreign_key: :responds_to_id
  has_one :reply, class_name: "Message", foreign_key: :responds_to_id

  after_create :alert_recipient

  private
  def alert_recipient
    Notifier.new_message(self.recipient, self.sender, self).deliver_now
  end
end
