class Customer < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_one :profile
  has_many :sent_messages, class_name: "Message", foreign_key: :sender_id
  has_many :received_messages, class_name: "Message", foreign_key: :recipient_id

  validates :date_of_birth, presence: true
  validates :username, uniqueness: true, presence: true
  after_create :create_profile

  MIN_AGE = 18

  def create_profile
    Profile.create!(:customer => self)
  end

  def gender
    type ? type.downcase : "female"
  end
end
