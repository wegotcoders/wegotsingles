$(function() {

  $('#ft_button').click(showFeetAndInches);
  $('#cm_button').click(showCM);

  var measurements = $('#measurements');
  var feet = $('#feet').clone();
  $('#feet').remove();
  var inches = $('#inches').clone();
  $('#inches').remove();
  var cm = $('#cm').clone();

  function showFeetAndInches() {
    measurements.html(feet).append(inches);
  }

  function showCM() {
    measurements.html(cm)
  }
})
