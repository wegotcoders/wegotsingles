Feature: Search within a distance

  @javascript
  Scenario: A customer searches for matches with a certain distance
    Given some profiles exist
      And the customer is signed in
      And the customer is on the profiles index page
    When the customer selects a distance
      And the customer searches
    Then they will see the profiles that match within that distance
