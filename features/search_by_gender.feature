Feature: Searching by gender

  @javascript
  Scenario: A customer searches for matches by gender

  Given some profiles exist
    And the customer is signed in
    And the customer is on the profiles index page
  When the customer selects a gender
    And the customer searches
  Then they will see the profiles that match the gender
