Feature: Profile Percentage Completed
  @javascript
  Scenario: A customer views how complete their profile is
    Given a profile exists
      And a customer logs in
    Given a customer is on a profile page
      Then a customer sees how complete their profile is
