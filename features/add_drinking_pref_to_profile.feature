Feature: Drinking preferences

  Scenario: A customer chooses their drinking preferences for their profile
  Given a profile exists
    And a religion exists
  When a customer visits the edit profile page
    And they enter valid details into the edit profile form
    And they click on submit
  Then the profile is updated
