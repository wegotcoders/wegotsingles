Feature: Forgotten Password 
 
  @javascript
  Scenario: A customer retreives a forgotten password
    Given a customer exists
    Given a customer is on the signin form
     Then a customer selects the forgotten password link
      And fills in a valid email address
    Then selects the send me reset password button
      And they should receive a new email to reset their password