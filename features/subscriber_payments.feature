Feature: Subscriber Payments

Scenario: A customer becomes a subscriber
  Given a customer exists
  Given a customer is on the signin form
    And and a customer signs using valid details
    And a customer selects the sign in button
  Then a customer should be taken to their profile page
  When we are on the payment page
    Then we should see the correct price for the plan



