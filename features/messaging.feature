Feature: Messaging

  Background:
    Given two customers exist

  Scenario: A customer views a message
    Given a message exists
    When a customer chooses to view the message
    Then they see the message subject displayed
      And they see the message body displayed
      And they see the sender's information displayed
  @wip
  Scenario: A customer replies to a message
    Given two messages exist
      And the customer is logged in
    When a customer chooses to view the message
      And a customer replies the message
    Then a new form is created to reply the original message
    When the customer fields the subject
      And the customer fields the body
      And the customer sends the reply
    Then a message reply is created

  Scenario: A customer views a list of received messages
    Given several messages exist
      And the customer is logged in
      And the customer is on the homepage
    When the customer chooses to see the messages
    Then they should be in the inbox page
      And see a list of their received messages

  Scenario: A customer reads a message and it no longer appears as unread
    Given several messages exist
      And the customer is logged in
      And the customer is on the inbox page
    Then they will see the number of unread messages in the header
      And they will see the unread messages marked as unread
    When the customer chooses to read a message
      And then return to the inbox page
    Then the message should no longer be marked as unread
