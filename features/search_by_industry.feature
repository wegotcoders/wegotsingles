Feature: Search by Industry
  @javascript
  Scenario: A customer searches by industry

  Given some profiles exist
    And the customer is signed in
    And the customer is on the profiles index page
  When the selects an industry
    And the customer searches
    Then they will see the profiles that match the industry
