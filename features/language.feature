Feature: Add languages to profile

Background:
  Given a profile exists
    And a customer visits the edit profile page

@javascript
Scenario:A customer adds a language to their profile
  When a customer chooses to add a language
  Then a new form field is displayed
  When the customer enters the language "English"
    And they click on submit
  Then a language is added in the customer profile

@javascript
Scenario: A customer adds several languages
  When a customer chooses to add a language
  Then a new form field is displayed
  When the customer enters the language "English"
    And a customer chooses to add a language
    And the customer enters the language "French"
    And a customer chooses to add a language
    And the customer enters the language "Spanish"
    And they click on submit
  Then three languages are added to their profile
