Feature: Edit profile biography

	Scenario: A customer edits their profile biography
	Given a profile exists
    And a religion exists
		And a profile has a biography
	When a customer visits the edit profile page
		And they enter valid details into the edit profile form
		And they click on submit
	Then the profile is updated

  @javascript
  Scenario: A customer edits their profile with height in cm
    Given a profile exists
      And a religion exists
    When a customer visits the edit profile page
      And they enter valid details into the edit profile form
      And they choose to enter their height in cm
      And they enter their height in cm
      And they click on submit
    Then their height is set correctly

  @javascript
  Scenario: A customer edits their profile with height in inches
    Given a profile exists
    And a religion exists
    When a customer visits the edit profile page
      And they enter valid details into the edit profile form
      And they choose to enter their height in feet and inches
      And they enter their height in feet and inches
      And they click on submit
    Then their height is set correctly
