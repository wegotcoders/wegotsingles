Feature: Search by Age

  @javascript
  Scenario: A customer searches by age

  Given some profiles exist
    And the customer is signed in
    And the customer is on the profiles index page
  When they select an age range
    And the customer searches
    Then they will see the profiles that match that age range
