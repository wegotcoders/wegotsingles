When(/^the customer selects a distance$/) do
  select "5km", from: "Distance"
  select "130", from: :max_age
end

Then(/^they will see the profiles that match within that distance$/) do
  expect(page).to have_content @profile_3.customer.first_name
  expect(page).not_to have_content @profile_2.customer.first_name
end
