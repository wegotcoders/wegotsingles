Given(/^a profile has a biography$/) do
  Profile.first.update!(biography: Faker::Lorem.paragraph)
end

Given(/^a religion exists$/) do
  @religion = Religion.create!(name: "agnostic")
end

When(/^a customer visits the edit profile page$/) do
  visit edit_profile_path(Profile.first)
end

When(/^they enter valid details into the edit profile form$/) do
	@text = Faker::Lorem.paragraph
  fill_in "Biography", with: @text
  select "college", from: "Education"
  Profile.first.religion = @religion
  Profile.first.save!
  select "agnostic", from: "Religion"
  check 'Smoker'
  select "capricorn", from: "Star Sign"
  select 'Never', from: "Drinks"
end

When(/^they click on submit$/) do
  click_on "Submit"
end

Then(/^the profile is updated$/) do
  expect(Profile.last.biography).to eq(@text)
  expect(Profile.last.education).to eq("college")
  expect(Profile.last.religion.name).to eq("agnostic")
  expect(Profile.first.biography).to eq(@text)
  expect(Profile.first.smoker).to eq(true)
  expect(Profile.first.drinks).to eq("Never")
  expect(Profile.first.star_sign).to eq("capricorn")
end

When(/^they choose to enter their height in cm$/) do
  choose "cm_button"
end

When(/^they enter their height in cm$/) do
  fill_in "profile_height", with: "182"
end

Then(/^their height is set correctly$/) do
  expect(Profile.first.height).to eq(182)
end

When(/^they choose to enter their height in feet and inches$/) do
  choose "ft_button"
end

When(/^they enter their height in feet and inches$/) do
  fill_in "Height(ft)", with: "5"
  fill_in "Height(in)", with: "12"
end

