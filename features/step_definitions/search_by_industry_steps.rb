When(/^the selects an industry$/) do
  select "Medicine", :from => "industry"
  select "18", :from => "min_age"
  select "120", :from => "max_age"
end

When(/^the customer searches$/) do
  click_button('search')
end

Then(/^they will see the profiles that match the industry$/) do
  expect(page).to have_content @profile_3.customer.first_name
  expect(page).not_to have_content @profile_2.customer.first_name
  expect(page).to have_content @profile_1.customer.first_name
end
