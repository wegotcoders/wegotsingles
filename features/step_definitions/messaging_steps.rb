Given(/^two customers exist$/) do
  @sender = Customer.create!(first_name: "Patty", username: "sexypatty", last_name: Faker::Name.last_name, type: "Female", email: "test1@test.com", password: "password1", password_confirmation: "password1", date_of_birth: Date.parse("Sat, 03 Dec 1980"))
  @recipient = Customer.create!(first_name: "Peter", username: "sexypeter", last_name: Faker::Name.last_name, type: "Male", email: "test2@test.com", password: "password2", password_confirmation: "password2", date_of_birth: Date.parse("Sat, 03 Dec 1980"))
end

Given(/^a message exists$/) do
  @message = Message.create!(subject: "Hi", body: "Hello. This is a real message.", sender: @sender, recipient: @recipient)
end

When(/^a customer chooses to view the message$/) do
  visit message_path(@message)
end

Then(/^they see the message subject displayed$/) do
  expect(page).to have_content("Hi")
end

Then(/^they see the message body displayed$/) do
  expect(page).to have_content "Hello. This is a real message."
end

Then(/^they see the sender's information displayed$/) do
  expect(page).to have_content @sender.username
end

Given(/^two messages exist$/) do
  @sender = Customer.create!(first_name: "Bob", last_name: "Smith", username: "sexy_bob", email: "test_bob@test.com", password: "password1", password_confirmation: "password1", date_of_birth: Date.parse("Sat, 03 Dec 1980"))
  @recipient = Customer.create!(first_name: "Sarah", last_name: "Jane", username: "sexy_sarah", email: "test_sarah@test.com", password: "password2", password_confirmation: "password2", date_of_birth: Date.parse("Sat, 03 Dec 1980"))
  @message = Message.create!(subject: "Hello Sarah", body: "How are you?", sender: @sender, recipient: @recipient)
end

When(/^a customer replies the message$/) do
  click_on "Reply"
end

Then(/^a new form is created to reply the original message$/) do
  expect(page).to have_css('input[id=message_subject]')
  expect(page).to have_css('input[id=message_body]')
end

When(/^the customer fields the subject$/) do
  fill_in "message_subject", :with => "Hello Bob"
end

When(/^the customer fields the body$/) do
  fill_in "message_body", :with => "I am fine, thank you. And you?"
end

When(/^the customer sends the reply$/) do
  click_button "send"
end

Then(/^a message reply is created$/) do
  expect(Message.where(:responds_to_id => @message.id))
end

Given(/^several messages exist$/) do
  @message_1 = Message.create!(subject: "Hi", body: "Hello. This is a real message.", sender: @sender, recipient: @recipient, read: nil)
  @message_2 = Message.create!(subject: "Hi again", body: "Hello. This is a real message.", sender: @sender, recipient: @recipient, read: nil)
  @message_3 = Message.create!(subject: "Why are you ignoring me?", body: "Hello. This is a real message.", sender: @sender, recipient: @recipient, read: true)
end

Given(/^the customer is logged in$/) do
  visit new_customer_session_path
  fill_in "Email", :with => "test2@test.com"
  fill_in "Password", :with => "password2"
  click_on "Log in"
end

Given(/^the customer is on the homepage$/) do
  visit root_path
end

When(/^the customer chooses to see the messages$/) do
  click_on "message-a-tag"
end

Then(/^they should be in the inbox page$/) do
  expect(current_path).to eq messages_path
end

Then(/^see a list of their received messages$/) do
  expect(page).to have_content("Hi")
  expect(page).to have_content("Hi again")
  expect(page).to have_content("Why are you ignoring me?")
end

Given(/^the customer is on the inbox page$/) do
  visit messages_path
end

Then(/^they will see the number of unread messages in the header$/) do
  within "#new-messages" do
    expect(page).to have_content 2
  end
end

Then(/^they will see the unread messages marked as unread$/) do
  expect(page).to have_content("Unread", count: 2)
end

When(/^the customer chooses to read a message$/) do
  click_on @message_1.subject
end

When(/^then return to the inbox page$/) do
  visit messages_path
end

Then(/^the message should no longer be marked as unread$/) do
  expect(page).to have_content("Unread", count: 1)
end
