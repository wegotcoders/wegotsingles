Given(/^the customer is signed in$/) do
  @customer = Customer.create!(first_name: "Patty", username: "sexypatty", last_name: Faker::Name.last_name, type: "Female", email: "testpatty@test.com", password: "password1", password_confirmation: "password1", :gender_pref => 'Female', date_of_birth: Date.parse("Sat, 03 Dec 1980"))
  @customer.profile.update!(:latitude => 3, :longitude => 3 )
  visit new_customer_session_path
  fill_in "Email", :with => "testpatty@test.com"
  fill_in "Password", :with => "password1"
  click_on "Log in"
end

When(/^they select an age range$/) do

  select "25", from: :min_age
  select "30", from: :max_age

end

Then(/^they will see the profiles that match that age range$/) do
  expect(page).to have_content @profile_3.customer.first_name
  expect(page).not_to have_content @profile_2.customer.first_name
end
