Given(/^we are on the payment page$/) do
  visit new_subscriber_path
  # expect(page).to have_content 'Thanks for choosing to become a premium member'
end
Then(/^we should see the correct price for the plan$/) do
  expect(page).to have_selector(:css, 'script[data-amount="999"]', visible: false, minimum: 1)
  expect(page).to have_selector(:css, 'script[src="https://checkout.stripe.com/checkout.js"]', visible: false, minimum: 1)
  expect(page).to have_selector(:css, 'script[data-description="A month\'s subscription"]', visible: false, minimum: 1)

end