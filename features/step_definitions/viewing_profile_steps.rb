Given(/^a profile exists$/) do

  Geocoder::Lookup::Test.add_stub(
    "London, UK", [
      {
        'latitude'     => 51.5073509,
        'longitude'    => -0.1277583,
        'address'      => 'London, UK',
        'state'        => 'London',
        'state_code'   => '',
        'country'      => 'UK',
        'country_code' => 'UL'
      }
    ]
  )

  @customer = Customer.create!(first_name: "Bob", last_name: "Smith", type: "Female", email: "test3@test.com", password: "password1", password_confirmation: "password1", date_of_birth: Date.parse("Sat, 03 Dec 1926"), :username => "username2")

  @customer.profile.update!(age: 89, :img_url => "http://www.image.com", city: "London", country: "UK")
  @profile = @customer.profile
end

When(/^a customer chooses to view the profile$/) do
  visit profile_path(@profile)
end

Then(/^they see the profile$/) do
  expect(page).to have_content @profile.customer.first_name
  expect(page).to have_content @profile.age
end
