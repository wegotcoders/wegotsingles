Then(/^a language is added in the customer profile$/) do
  expect(Profile.first.profile_languages.length).to eq(1)
  expect(ProfileLanguage.all.length).to eq(1)
  expect(Language.all.length).to eq(1)
end

When(/^a customer chooses to add a language$/) do
  click_link "Add Language"
end

Then(/^a new form field is displayed$/) do
  @i ||= 0
  expect(page).to have_css("input#profile_profile_languages_attributes_#{@i}_language_attributes_name")
end

When(/^the customer enters the language "(.*?)"$/) do |language|
  fill_in "profile_profile_languages_attributes_#{@i}_language_attributes_name", :with => language
  @i += 1
end

Then(/^three languages are added to their profile$/) do
  expect(Profile.first.languages.length).to eq(3)
  expect(Profile.first.languages.first.name).to eq("English")
  expect(Profile.first.languages.second.name).to eq("French")
  expect(Profile.first.languages.last.name).to eq("Spanish")
end


