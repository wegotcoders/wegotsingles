Given(/^some profiles exist$/) do

  Geocoder::Lookup::Test.add_stub(
    "London, UK", [
      {
        'latitude'     => 51.5073509,
        'longitude'    => -0.1277583,
        'address'      => 'London, UK',
        'state'        => 'London',
        'state_code'   => '',
        'country'      => 'UK',
        'country_code' => 'UL'
      }
    ]
  )

  Geocoder::Lookup::Test.add_stub(
    "Sydney, Australia", [
      {
        'latitude'     => -33.8674869,
        'longitude'    => 151.2069902,
        'address'      => 'Sydney NSW, Australia',
        'state'        => 'Sydney',
        'state_code'   => '',
        'country'      => 'Australia',
        'country_code' => 'AU'
      }
    ]
  )

  @customer_1 = Customer.create!(first_name: "Patty", last_name: Faker::Name.last_name, type: "Female", email: "test1@test.com", password: "password1", password_confirmation: "password1", date_of_birth: Date.parse("Sat, 03 Dec 1972"), :username => "username1")
  @customer_1.profile.update!(age: 43, industry: "Medicine", city: "London", country: "UK", ethnicity: "Asian", drinks: "Never")
  @profile_1 = @customer_1.profile

  @customer_2 = Customer.create!(first_name: "Peter", last_name: Faker::Name.last_name, type: "Male", email: "test2@test.com", password: "password1", password_confirmation: "password1", date_of_birth: Date.parse("Sat, 03 Dec 1980"), :username => "username2")
  @customer_2.profile.update!(age: 35, industry: "Advertising", city: "Sydney", country: "Australia")
  @profile_2 = @customer_2.profile

  @customer_3 = Customer.create!(first_name: "Mary", last_name: Faker::Name.last_name, type: "Female", email: "test3@test.com", password: "password1", password_confirmation: "password1", date_of_birth: Date.parse("Sat, 03 Dec 1986"), :username => "username3")
  @customer_3.profile.update!(age: 29, industry: "Medicine", city: "London", country: "UK", ethnicity: "Asian", drinks: "Never")
  @profile_3 = @customer_3.profile

end

Given(/^the customer is on the profiles index page$/) do
  visit profiles_path
end

When(/^the customer selects a gender$/) do
  select "female", from: "Gender"
  select "130", from: :max_age
end

Then(/^they will see the profiles that match the gender$/) do
  expect(page).to have_content @profile_3.customer.first_name
  expect(page).to have_content @profile_2.customer.first_name
end
