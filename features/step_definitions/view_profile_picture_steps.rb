Given(/^a customer logs in$/) do
  visit new_customer_session_path
  fill_in "Email", :with => "test3@test.com"
  fill_in "Password", :with => "password1"
  click_on "Log in"
end

Given(/^a customer is on a profile page$/) do
  visit profile_path(@profile)
  expect(current_path).to eq(profile_path(@profile))
end

Then(/^a customer sees the profile image$/) do
  expect(page).to have_css("img.profile-photo[src='#{@profile.img_url}']")
end
