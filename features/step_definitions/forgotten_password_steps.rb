Then(/^a customer selects the forgotten password link$/) do
  click_link "Forgot your password?"
end

Then(/^fills in a valid email address$/) do
  fill_in "Email", with: "test3@test.com"
end

Then(/^selects the send me reset password button$/) do
  click_on "Send me reset password instructions"
end

Then(/^they should receive a new email to reset their password$/) do
  expect(ActionMailer::Base.deliveries.length).to eq(1)
end
