When(/^the customer selects an ethnicity$/) do
  select "Hispanic", :from => "Ethnicity"
  select "130", from: :max_age
end

Then(/^they will see the profiles that match the ethnicity$/) do
  expect(page).to have_content @profile_3.customer.first_name
  expect(page).not_to have_content @profile_2.customer.first_name
end
