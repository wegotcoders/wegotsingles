Given(/^A customer is on the sign up page$/) do
  visit new_customer_registration_path
  expect(page).to have_content 'Sign up'
  expect(page).to have_content 'Over 1 New People Join Every Day!'
end

Given(/^they enter valid details$/) do
  fill_in "First name", :with => "Simone"
  fill_in "Last name", :with => "Knight"
  fill_in "Email", :with => "test@test.com"
  fill_in "Password", :with => "password"
  fill_in "Password confirmation", :with => "password"
  select "Female", :from => "Gender"
  select '1980', :from => 'customer_date_of_birth_1i'
  select 'December', :from => 'customer_date_of_birth_2i'
  select '3', :from => 'customer_date_of_birth_3i'
  fill_in "Username", :with => "username1"
end

Given(/^they enter a username$/) do
  fill_in "Username", :with => "username1"
end

Then(/^they select the signup button$/) do
  click_button "Sign up"
end

Then(/^the account is created$/) do
  expect(Customer.count).to eq(1)
  expect(Customer.last.email).to eq("test@test.com")
  expect(Customer.last.type).to eq("Female")
  expect(Customer.last.date_of_birth).to eq(Date.parse("Sat, 03 Dec 1980"))
  expect(Customer.last.username).to eq("username1")
end

Then(/^they are redirected to their profile page$/) do
  expect(page).to have_content 'Simone Knight'
end

Then(/^they are shown a flash message "(.*?)"$/) do |message|
  expect(page).to have_content(message)
end

Given(/^they enter a username that is already taken$/) do
  Customer.create!(:first_name => "Simone", :username => "username2", :password => "password1", :password_confirmation => "password1", :email => "test@test.com", :username => "username2", :date_of_birth => Date.parse("Sat, 03 Dec 2011"))
  fill_in "First name", :with => "Paul"
  fill_in "Last name", :with => "Peter"
  fill_in "Email", :with => "test2@test.com"
  fill_in "Password", :with => "password"
  fill_in "Password confirmation", :with => "password"
  select "Female", :from => "Gender"
  fill_in "Username", :with => "username2"
end

Then(/^the account is not created$/) do
  expect(Customer.count).to eq(1)
  expect(Customer.last.first_name).not_to eq("Paul")
  expect(Customer.last.first_name).to eq("Simone")
end

Given(/^they enter invalid details\(missing username\)$/) do
  fill_in "First name", :with => "Paul"
  fill_in "Last name", :with => "Peter"
  fill_in "Email", :with => "test2@test.com"
  fill_in "Password", :with => "password"
  fill_in "Password confirmation", :with => "password"
  select "Female", :from => "Gender"
end

Then(/^the account isnt created$/) do
  expect(Customer.count).to eq(0)
end
