Given(/^a customer exists$/) do
  @customer = Customer.create!(first_name: "Bob", last_name: "Smith", type: "Female", email: "test3@test.com", password: "password1", password_confirmation: "password1", username: "username1", date_of_birth: Date.parse("Sat, 03 Dec 1980"))
end

Given(/^a customer is on the signin form$/) do
  visit new_customer_session_path
  expect(page).to have_content "Log in"
end

Given(/^and a customer signs using valid details$/) do
  fill_in 'Email', with: "test3@test.com"
  fill_in 'Password', with: "password1"
end

Given(/^a customer selects the sign in button$/) do
  click_button "Log in"
end

Then(/^a customer should be taken to their profile page$/) do
  expect(page).to have_content @customer.first_name
end
