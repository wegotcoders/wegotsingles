Given(/^the customer is signs in$/) do
  visit new_customer_session_path
  fill_in "Email", :with => "test3@test.com"
  fill_in "Password", :with => "password1"
  click_on "Log in"
end

Then(/^a customer sees how complete their profile is$/) do
  expect(page).to have_content "Your profile is #{@profile.profile_completed}% complete"
end
