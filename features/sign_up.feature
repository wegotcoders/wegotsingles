Feature: Sign Up

  @javascript
  Scenario: A Woman signs up
    
    Given A customer is on the sign up page
      And they enter valid details
    Then they select the signup button
      And the account is created
      And they are redirected to their profile page

  @javascript
  Scenario: A user signs up with a username

    Given A customer is on the sign up page
      And they enter valid details
      And they enter a username
      And they select the signup button
    Then the account is created
      And they are redirected to their profile page

  Scenario: A user signs up with an already taken username

    Given A customer is on the sign up page
      And they enter valid details
      And they enter a username that is already taken
      And they select the signup button
    Then the account is not created
      And they are shown a flash message "Username has already been taken"
  
  @javascript
  Scenario: A user signs up without entering a username

    Given A customer is on the sign up page
      And they enter invalid details(missing username)
      And they select the signup button
    Then the account isnt created
      And they are shown a flash message "Username can't be blank"
