Feature: Searching by ethnicity

 @javascript
  Scenario: A customer searches for matches by ethnicity

  Given some profiles exist
    And the customer is signed in
    And the customer is on the profiles index page
  When the customer selects an ethnicity
    And the customer searches
  Then they will see the profiles that match the ethnicity
