Feature: View Profile Picture

  Background:
  Given a profile exists

  Scenario: A customer views a profile picture
    Given a customer is on a profile page
      When a customer logs in
      Then a customer sees the profile image
