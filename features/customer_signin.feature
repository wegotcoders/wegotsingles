Feature: Customer Signin
@javascript
Scenario: A customer signs in uses valid details
  Given a customer exists
  Given a customer is on the signin form
    And and a customer signs using valid details
    And a customer selects the sign in button
  Then a customer should be taken to their profile page
