Feature: Viewing a profile

  Scenario: A customer views someone's profile

  Given a profile exists
  When a customer chooses to view the profile
    Then they see the profile
